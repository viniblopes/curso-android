package com.vbllopes.aluraviagens.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.vbllopes.aluraviagens.R;
import com.vbllopes.aluraviagens.model.Pacote;
import com.vbllopes.aluraviagens.util.DiasUtil;
import com.vbllopes.aluraviagens.util.MoedaUtil;
import com.vbllopes.aluraviagens.util.ResourcesUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ListaPacotesAdapter extends BaseAdapter {

    private final List<Pacote> pacotes;
    private final Context context;

    public ListaPacotesAdapter(List<Pacote> pacotes, Context context){
        this.pacotes = pacotes;
        this.context = context;
    }

    @Override
    public int getCount() {
        return pacotes.size();
    }

    @Override
    public Object getItem(int position) {
        return pacotes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View viewCriada = LayoutInflater.from(context).inflate(R.layout.item_pacote, parent, false);

        Pacote pacote = pacotes.get(position);
        mostraImagem(viewCriada, pacote);
        mostraLocal(viewCriada, pacote);
        mostraPreco(viewCriada, pacote);
        mostraDias(viewCriada, pacote);

        return viewCriada;
    }

    private void mostraDias(View viewCriada, Pacote pacote) {
        TextView dias = viewCriada.findViewById(R.id.item_pacote_dias);
        dias.setText(DiasUtil.formataDiasEmTexto(pacote.getDias()));
    }

    private void mostraPreco(View viewCriada, Pacote pacote) {
        TextView preco = viewCriada.findViewById(R.id.item_pacote_preco);
        preco.setText(MoedaUtil.formataPreco(pacote.getPreco()));
    }

    private void mostraImagem(View viewCriada, Pacote pacote) {
        ImageView imagem = viewCriada.findViewById(R.id.item_pacote_imagem);
        imagem.setImageDrawable(ResourcesUtil.buscaImagem(context, pacote.getImagem()));
    }

    private void mostraLocal(View viewCriada, Pacote pacote) {
        TextView cidade = viewCriada.findViewById(R.id.item_pacote_cidade);
        cidade.setText(pacote.getLocal());
    }
}
