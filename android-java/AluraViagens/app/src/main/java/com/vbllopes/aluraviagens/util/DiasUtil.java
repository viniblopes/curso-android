package com.vbllopes.aluraviagens.util;

public class DiasUtil {

    public static final String DIAS_PLURAL = " dias";
    public static final String DIAS_SINGULAR = " dia";

    public static String formataDiasEmTexto(int quantidadeDeDias){
        return quantidadeDeDias + (quantidadeDeDias > 1 ? DIAS_PLURAL : DIAS_SINGULAR);
    }
}
