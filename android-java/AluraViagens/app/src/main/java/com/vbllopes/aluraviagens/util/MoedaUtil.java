package com.vbllopes.aluraviagens.util;

import androidx.annotation.NonNull;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class MoedaUtil {

    public static final String LANGUAGE = "pt";
    public static final String COUNTRY = "br";

    @NonNull
    public static String formataPreco(BigDecimal precoPacote) {
        NumberFormat formatoBrasileiro = DecimalFormat.getCurrencyInstance(new Locale(LANGUAGE, COUNTRY));
        return formatoBrasileiro.format(precoPacote);
    }
}
