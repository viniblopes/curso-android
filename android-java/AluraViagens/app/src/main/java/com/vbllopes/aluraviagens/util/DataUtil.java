package com.vbllopes.aluraviagens.util;

import android.widget.TextView;
import com.vbllopes.aluraviagens.R;
import com.vbllopes.aluraviagens.model.Pacote;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DataUtil {
    public static String mostraPeriodo(Pacote pacoteSaoPaulo) {
        Calendar dataIda = Calendar.getInstance();
        Calendar dataVolta = Calendar.getInstance();
        dataVolta.add(Calendar.DATE, pacoteSaoPaulo.getDias());
        SimpleDateFormat formatoBrasileiro = new SimpleDateFormat("dd/MM");
        String dataFormatadaIda = formatoBrasileiro.format(dataIda.getTime());
        String dataFormatadaVolta = formatoBrasileiro.format(dataVolta.getTime());
        return dataFormatadaIda + " - "
                + dataFormatadaVolta + " de "
                + dataVolta.get(Calendar.YEAR);
    }
}
