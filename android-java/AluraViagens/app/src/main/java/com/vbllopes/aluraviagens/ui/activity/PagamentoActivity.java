package com.vbllopes.aluraviagens.ui.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.vbllopes.aluraviagens.R;
import com.vbllopes.aluraviagens.model.Pacote;
import com.vbllopes.aluraviagens.util.MoedaUtil;

import java.math.BigDecimal;

import static com.vbllopes.aluraviagens.ui.activity.PacoteActivityConstantes.CHAVE_PACOTE;

public class PagamentoActivity extends AppCompatActivity {

    public static final String TITULO_APPBAR = "Pagamento";
    Pacote pacote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagamento);
        setTitle(TITULO_APPBAR);

        carregaPacoteRecebido();
        configuraBotaoPagamento();
    }

    private void carregaPacoteRecebido() {
        Intent intent = getIntent();
        if (intent.hasExtra(CHAVE_PACOTE)) {
            pacote = (Pacote) intent.getSerializableExtra(CHAVE_PACOTE);
            mostraPreco(pacote);
        }
    }

    private void configuraBotaoPagamento() {
        Button botaoFinalizaCompra = findViewById(R.id.pagamento_finalizar_compra);
        botaoFinalizaCompra.setOnClickListener(v -> vaiParaResumoCompra());
    }

    private void vaiParaResumoCompra() {
        Intent intent = new Intent(PagamentoActivity.this, ResumoCompraActivity.class);
        intent.putExtra(CHAVE_PACOTE, pacote);
        startActivity(intent);
    }

    private void mostraPreco(Pacote pacote) {
        TextView preco = findViewById(R.id.pagamento_valor_compra);
        String moedaBrasileira = MoedaUtil.formataPreco(pacote.getPreco());
        preco.setText(moedaBrasileira);
    }
}
