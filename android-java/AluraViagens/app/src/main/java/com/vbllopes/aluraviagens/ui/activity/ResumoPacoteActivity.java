package com.vbllopes.aluraviagens.ui.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.vbllopes.aluraviagens.R;
import com.vbllopes.aluraviagens.model.Pacote;
import com.vbllopes.aluraviagens.util.DataUtil;
import com.vbllopes.aluraviagens.util.DiasUtil;
import com.vbllopes.aluraviagens.util.MoedaUtil;
import com.vbllopes.aluraviagens.util.ResourcesUtil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.vbllopes.aluraviagens.ui.activity.PacoteActivityConstantes.CHAVE_PACOTE;

public class ResumoPacoteActivity extends AppCompatActivity {

    Pacote pacote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumo_pacote);

        carregaPacoteRecebido();
        inicializaCampos();
        configuraBotao();

    }

    private void carregaPacoteRecebido() {
        Intent intent = getIntent();
        if (intent.hasExtra(CHAVE_PACOTE)){
            pacote = (Pacote) intent.getSerializableExtra(CHAVE_PACOTE);
        }
    }

    private void inicializaCampos() {
        mostraLocal(pacote);
        mostraImagem(pacote);
        mostraDias(pacote);
        mostraPreco(pacote);
        mostraPeriodo(pacote);
    }

    private void configuraBotao() {
        Button botaoFinalizaCompra = findViewById(R.id.resumo_pacote_realiza_pagamento);
        botaoFinalizaCompra.setOnClickListener(v -> vaiParaPagamento());

    }

    private void vaiParaPagamento() {
        Intent intent = new Intent(ResumoPacoteActivity.this, PagamentoActivity.class);
        intent.putExtra(CHAVE_PACOTE, pacote);
        startActivity(intent);
    }

    private void mostraPeriodo(Pacote pacoteSaoPaulo) {
        TextView data = findViewById(R.id.resumo_pacote_dias);

        String dataFormatadaDaViagem = DataUtil.mostraPeriodo(pacoteSaoPaulo);
        data.setText(dataFormatadaDaViagem);
    }

    private void mostraPreco(Pacote pacoteSaoPaulo) {
        TextView preco = findViewById(R.id.resumo_pacote_preco);
        String moedaBrasileira = MoedaUtil
                .formataPreco(pacoteSaoPaulo.getPreco());
        preco.setText(moedaBrasileira);
    }

    private void mostraDias(Pacote pacoteSaoPaulo) {
        TextView dias = findViewById(R.id.resumo_pacote_periodo);
        String diasEmTexto = DiasUtil.formataDiasEmTexto(pacoteSaoPaulo.getDias());
        dias.setText(diasEmTexto);
    }

    private void mostraImagem(Pacote pacoteSaoPaulo) {
        ImageView imagem = findViewById(R.id.resumo_pacote_imagem);
        Drawable drawableDoPacote = ResourcesUtil
                .buscaImagem(this, pacoteSaoPaulo.getImagem());
        imagem.setImageDrawable(drawableDoPacote);
    }

    private void mostraLocal(Pacote pacoteSaoPaulo) {
        TextView local = findViewById(R.id.resumo_pacote_local);
        local.setText(pacoteSaoPaulo.getLocal());
    }
}
