package com.vbllopes.aluraviagens.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import com.vbllopes.aluraviagens.R;
import com.vbllopes.aluraviagens.dao.PacoteDAO;
import com.vbllopes.aluraviagens.model.Pacote;
import com.vbllopes.aluraviagens.ui.adapter.ListaPacotesAdapter;

import java.util.List;

import static com.vbllopes.aluraviagens.ui.activity.PacoteActivityConstantes.CHAVE_PACOTE;

public class ListaPacotesActivity extends AppCompatActivity {

    ListView listViewPacotes;
    List<Pacote> listaDePacotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pacotes);
        setTitle(R.string.title_activity_lista_pacotes);

        configuraListView();

    }

    private void configuraListView() {
        this.listViewPacotes = findViewById(R.id.lista_pacotes_listview);
        this.listaDePacotes = new PacoteDAO().lista();
        this.listViewPacotes.setAdapter(new ListaPacotesAdapter(listaDePacotes, this));
        this.listViewPacotes.setOnItemClickListener((parent, view, position, id) -> vaiParaResumoPacote(position));
    }

    private void vaiParaResumoPacote(int position) {
        Intent intent = new Intent(ListaPacotesActivity.this, ResumoPacoteActivity.class);
        intent.putExtra(CHAVE_PACOTE, listaDePacotes.get(position));
        startActivity(intent);
    }

}
