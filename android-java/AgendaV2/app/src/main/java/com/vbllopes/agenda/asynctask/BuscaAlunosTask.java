package com.vbllopes.agenda.asynctask;

import android.os.AsyncTask;
import com.vbllopes.agenda.database.dao.AlunoDAO;
import com.vbllopes.agenda.model.Aluno;
import com.vbllopes.agenda.ui.adapter.ListaAlunosAdapter;

import java.util.List;

public class BuscaAlunosTask extends AsyncTask<Void, Void, List<Aluno>> {
    private final AlunoDAO dao;
    private final ListaAlunosAdapter adapter;

    public BuscaAlunosTask(AlunoDAO dao, ListaAlunosAdapter adapter) {
        this.dao = dao;
        this.adapter = adapter;
    }

    @Override
    protected List<Aluno> doInBackground(Void... voids) {
        return dao.getAlunos();
    }

    @Override
    protected void onPostExecute(List<Aluno> todosAlunos) {
        super.onPostExecute(todosAlunos);
        adapter.atualiza(todosAlunos);
    }
}
