package com.vbllopes.agenda.asynctask;

import android.os.AsyncTask;
import com.vbllopes.agenda.database.dao.TelefoneDAO;
import com.vbllopes.agenda.model.Aluno;
import com.vbllopes.agenda.model.Telefone;

import java.util.List;

public class BuscaTodosTelefonesDoAlunoTask extends AsyncTask<Void, Void, List<Telefone>> {
    private final TelefoneDAO telefoneDAO;
    private final Aluno aluno;
    private final TelefonesDoAlunoEncontradosListener listener;

    public BuscaTodosTelefonesDoAlunoTask(TelefoneDAO telefoneDAO, Aluno aluno, TelefonesDoAlunoEncontradosListener listener) {
        this.telefoneDAO = telefoneDAO;
        this.aluno = aluno;
        this.listener = listener;
    }

    @Override
    protected List<Telefone> doInBackground(Void... voids) {
        telefoneDAO.buscaTodosTelefoneDoAluno(aluno.getId());
        return telefoneDAO.buscaTodosTelefoneDoAluno(aluno.getId());
    }

    @Override
    protected void onPostExecute(List<Telefone> telefones) {
        super.onPostExecute(telefones);
        listener.quandoEncontrado(telefones);
    }

    public interface TelefonesDoAlunoEncontradosListener{
        void quandoEncontrado(List<Telefone> telefones);
    }
}
