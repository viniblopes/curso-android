package com.vbllopes.agenda.database.dao;

import androidx.room.*;
import com.vbllopes.agenda.model.Telefone;

import java.util.List;

@Dao
public interface TelefoneDAO {
    @Query("SELECT t.* FROM Telefone t " +
            "WHERE t.alunoId = :alunoId LIMIT 1")
    Telefone buscaPrimeiroTelefoneDoAluno(int alunoId);

    @Insert
    void salva(Telefone... telefones);


    @Query("SELECT t.* FROM Telefone t " +
            "WHERE t.alunoId = :alunoId")
    List<Telefone> buscaTodosTelefoneDoAluno(int alunoId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void atualiza(Telefone... telefones);
}
