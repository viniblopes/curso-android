package com.vbllopes.agenda.database;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import com.vbllopes.agenda.database.converter.ConversorCalendar;
import com.vbllopes.agenda.database.converter.ConversorTipoTelefone;
import com.vbllopes.agenda.database.dao.AlunoDAO;
import com.vbllopes.agenda.database.dao.TelefoneDAO;
import com.vbllopes.agenda.model.Aluno;
import com.vbllopes.agenda.model.Telefone;

import static com.vbllopes.agenda.database.AgendaMigrations.TODAS_MIGRATIONS;

@Database(entities = {Aluno.class, Telefone.class}, version = 6, exportSchema = false)
@TypeConverters({ConversorCalendar.class, ConversorTipoTelefone.class})
public abstract class AgendaDatabase extends RoomDatabase {

    public static final String NOME_BANCO_DE_DADOS = "agenda.db";

    public abstract AlunoDAO getAlunoDao();

    public abstract TelefoneDAO getTelefoneDAO();

    public static AgendaDatabase getInstance(Context context) {
        return Room
                .databaseBuilder(context, AgendaDatabase.class, NOME_BANCO_DE_DADOS)
                .addMigrations(TODAS_MIGRATIONS)
                .build();
    }
}
