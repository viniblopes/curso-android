package com.vbllopes.agenda.database.dao;

import androidx.room.*;
import com.vbllopes.agenda.model.Aluno;

import java.util.List;

@Dao
public interface AlunoDAO {

    @Insert
    Long salva(Aluno aluno);

    @Delete
    void remove(Aluno aluno);

    @Query("SELECT * FROM Aluno")
    List<Aluno> getAlunos();

    @Update
    void edita(Aluno aluno);
}
