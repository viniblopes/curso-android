package com.vbllopes.agenda.model;

public enum TipoTelefone {
    FIXO, CELULAR
}
