package com.vbllopes.ceep.ui.recyclerview.adapter.listener;

import com.vbllopes.ceep.model.Nota;

public interface OnItemClickListener {

    void onItemClick(Nota nota, int posicao);
}
