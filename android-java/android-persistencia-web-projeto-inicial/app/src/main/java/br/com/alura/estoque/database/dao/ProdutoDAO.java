package br.com.alura.estoque.database.dao;

import java.util.List;

import androidx.room.*;
import br.com.alura.estoque.model.Produto;

@Dao
public interface ProdutoDAO {

    @Insert
    long salva(Produto produto);

    @Update
    void atualiza(Produto produto);

    @Query("SELECT * FROM Produto")
    List<Produto> buscaTodos();

    @Query("SELECT * FROM Produto WHERE id = :id")
    Produto buscaProduto(long id);

    @Delete
    void remove(Produto produto);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void salva(List<Produto> produtosNovos);
}
