package br.com.alura.estoque.retrofit.callback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class BaseCallbackSemResposta<T>  implements Callback<T> {
    private final RespostaCallBack callback;

    public BaseCallbackSemResposta(RespostaCallBack callback) {
        this.callback = callback;
    }


    @Override
    @EverythingIsNonNull
    public void onResponse(Call<T> call, Response<T> resposta) {
        if(resposta.isSuccessful()){
            callback.quandoSucesso();
        } else {
            callback.quandoFalha(MensagensCallback.MENSAGEM_ERRO_RESPOSTA_NAO_SUCEDIDA);
        }
    }

    @Override
    @EverythingIsNonNull
    public void onFailure(Call<T> call, Throwable t) {
        callback.quandoFalha(MensagensCallback.MENSAGEM_ERRO_FALHA_COMUNICACAO + t.getMessage());
    }

    public interface RespostaCallBack {
        void quandoSucesso();
        void quandoFalha(String erro);
    }
}
