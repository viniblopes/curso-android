package br.com.alura.estoque.retrofit.service;

import br.com.alura.estoque.model.Produto;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface ProdutoService {

    @GET("produto")
    Call<List<Produto>> buscaTodos();

    @POST("produto")
    Call<Produto> salva(@Body Produto produto);

    @PUT("produto/{id}")
    Call<Produto> edita(@Path("id") long id,
                        @Body Produto produto);

    @DELETE("produto/{id}")
    Call<Void> remove(@Path("id")long id);
}
