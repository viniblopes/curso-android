package br.com.alura.estoque.retrofit.callback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class BaseCallbackComResposta<T>  implements Callback<T> {
    private final RespostaCallBack<T> callback;

    public BaseCallbackComResposta(RespostaCallBack<T> callback) {
        this.callback = callback;
    }


    @Override
    @EverythingIsNonNull
    public void onResponse(Call<T> call, Response<T> resposta) {
        if(resposta.isSuccessful()){
            T resultado = resposta.body();
            if(resultado != null){
                callback.quandoSucesso(resultado);
            }
        } else {
            callback.quandoFalha(MensagensCallback.MENSAGEM_ERRO_RESPOSTA_NAO_SUCEDIDA);
        }
    }

    @Override
    @EverythingIsNonNull
    public void onFailure(Call<T> call, Throwable t) {
        callback.quandoFalha(MensagensCallback.MENSAGEM_ERRO_FALHA_COMUNICACAO + t.getMessage());
    }

    public interface RespostaCallBack<T> {
        void quandoSucesso(T resultado);
        void quandoFalha(String erro);
    }
}
