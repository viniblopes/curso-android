package com.example.financask.extension

import java.text.SimpleDateFormat
import java.util.*

private val primeiroCaracter = 0

fun String.limitaEmAte(caracteres: Int): String {
    if(this.length > caracteres){
        return "${this.substring(primeiroCaracter, caracteres)}..."
    }
    return this
}

fun String.converteParaCalendar(): Calendar {
    val formatoBrasileiro = SimpleDateFormat("dd/MM/yyyy")
    val dataConvertida: Date = formatoBrasileiro.parse(this)
    val data = Calendar.getInstance()
    data.time = dataConvertida
    return data
}